/* 
 * Author: M. Samil Atesoglu 
 * Date Created:  14.03.2019
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

typedef enum
{
    false,
    true
} bool;

typedef struct
{
    int id;
    char *filename;
    char *content;
} File;

/* Constants */
#define NOT_FOUND -1

/* Defining utility macros. */
#define EQUALS(str1, str2) ((strcmp(str1, str2) == 0) ? true : false)
#define STR_REALLOC(str, size) (char *)realloc(str, (size) * sizeof(char))
#define STR_ALLOC(size) (char *)calloc(size, sizeof(char))
#define FILE_REPO_REALLOC(repo, size) (File *)realloc(repo, (size) * sizeof(File))
#define CONTAINS(str, sub) (strstr(str, sub) != NULL)

/* 
 * A function that returns a char pointer to the content of the file 
 * with given name in the working directory, on disk.
 */
char *read_file(char *filename)
{
    FILE *f = fopen(filename, "rb");
    if (f == NULL)
    {
        char msg[100];
        sprintf(msg, "Error while opening the file %s", filename);
        perror(msg);
        exit(EXIT_FAILURE);
    }

    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET); /* To reset seek. */

    char *string = STR_ALLOC(fsize + 1);
    fread(string, fsize, 1, f);
    fclose(f);

    string[fsize] = '\0';

    return string;
}

/* Counts number of non-overlapping times given substring mentioned in string. */
int count_substring(char *str, char *sub)
{
    int count = 0, len = strlen(sub);

    while (*str != '\0')
    {
        if (strncmp(str++, sub, len))
            continue;
        str += len - 1;
        count++;
    }
    return count;
}

/* 
 * Splits the given string with supplied delimiter.
 * Returns a pointer to array of strings.
 * 
 * Warning!
 * The supplied string must be dynamically allocated.
 * And it will be modified.
 */
char **split(char *str, char *delimiter)
{
    int i = 0;
    char **array = (char **)malloc((count_substring(str, delimiter) + 1) * sizeof(char *));
    char *p = strtok(str, delimiter);

    while (p != NULL)
    {
        array[i++] = p;
        p = strtok(NULL, delimiter);
    }

    return array;
}

/* 
 * Removes trailing and leading whitespace characters of the given string.
 * This function modifies the given string, reallocates and returns a new pointer
 * to that string.
 */
char *trim(char *str)
{
    /* For leading whitespaces. */
    int i;
    while (str[0] == ' ')
    {
        int length = strlen(str);
        for (i = 0; i < length; i++)
        {
            str[i] = str[i + 1];
        }
        str = STR_REALLOC(str, strlen(str));
    }

    /* For trailing whitespaces. */
    while (str[strlen(str) - 1] == ' ')
    {
        str[strlen(str) - 1] = '\0';
        str = STR_REALLOC(str, strlen(str));
    }

    return str;
}

/* Replaces all occurences of given substring in string with given replacement. */
char *replace_str_with(char *str, char *to_be_replaced, char *replace_str)
{
    char *ptr = strstr(str, to_be_replaced);
    int len_to_be_rep = strlen(to_be_replaced);
    int len_rep = strlen(replace_str);
    while (ptr != NULL)
    {
        char *str_tail = ptr + len_to_be_rep;
        char *temp_tail = STR_ALLOC(strlen(str_tail) + 1);
        strcpy(temp_tail, str_tail);
        strcpy(ptr, replace_str);
        strcat(str, temp_tail);
        free(temp_tail);
        ptr = strstr(ptr + len_rep, to_be_replaced);
    }
    return str;
}

/* 
 * Returns a pointer to the nearest right-side parameter of a
 * given parameter in given command. 
 * 
 * "..." in function definition indicates that this function can take 
 * additional variables and has no fixed parameter list.
 */
char *end_param(char *param, char *command, int number_of_other_params, ...)
{
    va_list arg_ptr;
    va_start(arg_ptr, number_of_other_params);
    char *end_param_ptr = strchr(command, '\0');
    char *curr_other_param = va_arg(arg_ptr, char *);
    char *param_ptr = strstr(command, param);
    int i;
    for (i = 0; i < number_of_other_params; i++)
    {
        char *curr_ptr = strstr(command, curr_other_param);
        if (curr_ptr > param_ptr && end_param_ptr > curr_ptr)
            end_param_ptr = curr_ptr;
        curr_other_param = va_arg(arg_ptr, char *);
    }
    va_end(arg_ptr);
    return end_param_ptr;
}

/* Returns index of given filename in "File Repository". */
int index_of(File **file_list, int number_of_files, char *name)
{
    int i;
    bool found = false;
    for (i = 0; i < number_of_files; i++)
    {
        if (EQUALS((*file_list)[i].filename, name))
        {
            found = true;
            break;
        }
    }

    if (found)
        return i;
    else
        return NOT_FOUND;
}

/* Creates a new file in "File Repository" and reallocates it. */
void create_file(File **file_list, int *number_of_files, int *file_creations, char *name, char *content)
{
    int i = index_of(file_list, *number_of_files, name);
    if (i == NOT_FOUND)
    {
        (*file_list) = FILE_REPO_REALLOC((*file_list), (*number_of_files + 1));
        (*file_list)[*number_of_files].content = content;
        (*file_list)[*number_of_files].filename = name;
        (*file_list)[*number_of_files].id = ++(*file_creations);
        (*number_of_files)++;
    }
    else
        printf("[ERROR] File with the name \"%s\" already exists in the repository.\n", name);
}

/* Deletes a file with given name in the "File Repository" and reallocates the list. */
void delete_file(File **file_list, int *number_of_files, char *name)
{
    int i = index_of(file_list, *number_of_files, name);
    if (i != NOT_FOUND)
    {
        for (; i < *number_of_files - 1; i++)
        {
            (*file_list)[i] = (*file_list)[i + 1];
        }
        *file_list = FILE_REPO_REALLOC(*file_list, --(*number_of_files));
    }
    else
        printf("[ERROR] File with the name \"%s\" not found.\n", name);
}

/* Appends text to the file with given name. */
void append_text(File **file_list, int number_of_files, char *name, char *to_be_appended)
{
    int i = index_of(file_list, number_of_files, name);
    if (i != NOT_FOUND)
    {
        int length = strlen((*file_list)[i].content) + strlen(to_be_appended) + 1;
        char *new_str = STR_ALLOC(length);
        strcat(new_str, (*file_list)[i].content);
        strcat(new_str, to_be_appended);
        (*file_list)[i].content = new_str;
        free(name);
        free(to_be_appended);
    }
    else
        printf("[ERROR] File with the name \"%s\" not found.\n", name);
}

/* Removes text from file with given name. */
void remove_text(File **file_list, int number_of_files, char *name, int start, int length)
{
    int i = index_of(file_list, number_of_files, name);
    if (i != NOT_FOUND)
    {
        if (start < 0)
            printf("[ERROR] Start index \"-s %d\" cannot be smaller than zero.\n", start);
        else if (length < 0)
            printf("[ERROR] Length \"-l %d\" cannot be smaller than zero.\n", length);
        else if (length > strlen((*file_list)[i].content) - start)
            printf("[ERROR] Length \"-l %d\" cannot be greater than the start index plus the length of the file content.\n", length);
        else
        {
            char *str_tail = (*file_list)[i].content + start + length;
            char *str_head = STR_ALLOC(start + 1);
            strncpy(str_head, (*file_list)[i].content, start);
            str_head[start] = '\0';
            str_head = STR_REALLOC(str_head, strlen(str_head) + strlen(str_tail) + 1);
            strcat(str_head, str_tail);
            free((*file_list)[i].content);
            (*file_list)[i].content = str_head;
        }
    }
    else
        printf("[ERROR] File with the name \"%s\" not found.\n", name);
    free(name);
}

/* Replaces all occurences of a text in the file with given name with supplied string. */
void replace_text(File **file_list, int number_of_files, char *name, char *to_be_replaced, char *replace_str)
{
    int i = index_of(file_list, number_of_files, name);
    if (i != NOT_FOUND)
    {
        (*file_list)[i].content = replace_str_with((*file_list)[i].content, to_be_replaced, replace_str);
    }
    else
        printf("[ERROR] File with the name \"%s\" not found.\n", name);
}

void parse_create_command(File **file_list, int *number_of_files, int *file_creations, char *command)
{
    char *name = STR_ALLOC(strlen(command));
    char *content = STR_ALLOC(strlen(command));

    char *n_ptr = strstr(command, "-n");
    char *t_ptr = strstr(command, "-t");

    if (n_ptr < t_ptr)
    {
        strncpy(name, n_ptr + 3, t_ptr - n_ptr - 3);
        name[t_ptr - n_ptr - 4] = '\0';
        strcpy(content, t_ptr + 3);
    }
    else
    {
        strncpy(content, t_ptr + 3, n_ptr - t_ptr - 3);
        content[n_ptr - t_ptr - 4] = '\0';
        strcpy(name, n_ptr + 3);
    }

    if (strlen(content) == 0)
    {
        strcpy(content, "Empty File");
    }

    name = STR_REALLOC(name, strlen(name) + 1);
    content = STR_REALLOC(content, strlen(content) + 1);

    name = trim(name);

    create_file(file_list, number_of_files, file_creations, name, content);
}

void parse_delete_command(File **file_list, int *number_of_files, char *command)
{
    char *name = STR_ALLOC(strlen(command));
    char *n_ptr = strstr(command, "-n");
    strcpy(name, n_ptr + 3);
    name = STR_REALLOC(name, strlen(name) + 1);
    name = trim(name);
    delete_file(file_list, number_of_files, name);
    free(name);
}

void parse_append_command(File **file_list, int number_of_files, char *command)
{
    char *name = STR_ALLOC(strlen(command));
    char *content = STR_ALLOC(strlen(command));

    char *n_ptr = strstr(command, "-n");
    char *t_ptr = strstr(command, "-t");

    if (n_ptr < t_ptr)
    {
        strncpy(name, n_ptr + 3, t_ptr - n_ptr - 3);
        name[t_ptr - n_ptr - 4] = '\0';
        strcpy(content, t_ptr + 3);
    }
    else
    {
        strncpy(content, t_ptr + 3, n_ptr - t_ptr - 3);
        content[n_ptr - t_ptr - 4] = '\0';
        strcpy(name, n_ptr + 3);
    }

    name = STR_REALLOC(name, strlen(name) + 1);
    content = STR_REALLOC(content, strlen(content) + 1);

    name = trim(name);

    append_text(file_list, number_of_files, name, content);
}

void parse_remove_command(File **file_list, int number_of_files, char *command)
{
    char *n_ptr = strstr(command, "-n");
    char *s_ptr = strstr(command, "-s");

    int start;
    int length;
    char *name = STR_ALLOC(strlen(command));

    sscanf(s_ptr, "-s %d -l %d", &start, &length);
    strncpy(name, n_ptr + 3, s_ptr - n_ptr - 3);
    name[s_ptr - n_ptr - 4] = '\0';

    name = STR_REALLOC(name, strlen(name) + 1);

    name = trim(name);

    remove_text(file_list, number_of_files, name, start, length);
}

void parse_replace_command(File **file_list, int number_of_files, char *command)
{
    char *n_ptr = strstr(command, " -n ");
    char *ow_ptr = strstr(command, " -ow ");
    char *nw_ptr = strstr(command, " -nw ");

    int end_n_i = end_param(" -n ", command, 2, " -ow ", " -nw ") - n_ptr - 4;
    int end_ow_i = end_param(" -ow ", command, 2, " -nw ", " -n ") - ow_ptr - 5;
    int end_nw_i = end_param(" -nw ", command, 2, " -ow ", " -n ") - nw_ptr - 5;

    char *name = STR_ALLOC(strlen(command));
    char *old_word = STR_ALLOC(strlen(command));
    char *new_word = STR_ALLOC(strlen(command));

    strncpy(name, n_ptr + 4, end_n_i);
    strncpy(old_word, ow_ptr + 5, end_ow_i);
    strncpy(new_word, nw_ptr + 5, end_nw_i);

    name[end_n_i] = '\0';
    old_word[end_ow_i] = '\0';
    new_word[end_nw_i] = '\0';

    old_word = STR_REALLOC(old_word, strlen(old_word) + 1);
    new_word = STR_REALLOC(new_word, strlen(new_word) + 1);
    name = STR_REALLOC(name, strlen(name) + 1);

    name = trim(name);

    replace_text(file_list, number_of_files, name, old_word, new_word);
}

void parse_print_command(File **file_list, int number_of_files, char *command)
{
    if (CONTAINS(command, " -a"))
    {
        /* Print the filenames. */
        int i;
        for (i = 0; i < number_of_files; i++)
        {
            printf("Filename %d: %s\n", (*file_list)[i].id, (*file_list)[i].filename);
        }
    }
    else if (CONTAINS(command, " -e "))
    {
        /* Print the content of the files which have the given extension. */
        char *extension = strstr(command, " -e ") + 4;
        int i;
        for (i = 0; i < number_of_files; i++)
        {
            char *curr_extension = strstr((*file_list)[i].filename, ".");
            if (EQUALS(extension, curr_extension + 1))
            {
                int naked_filename_len = strlen((*file_list)[i].filename) - strlen(curr_extension);
                char *naked_filename = STR_ALLOC(naked_filename_len + 1);

                strncpy(naked_filename, (*file_list)[i].filename, naked_filename_len);
                naked_filename[naked_filename_len] = '\0';

                printf("Filename %d: %s\n", (*file_list)[i].id, naked_filename);
                printf("Text: %s\n", (*file_list)[i].content);

                free(naked_filename);
            }
        }
    }
    else if (EQUALS(command, "print -c"))
    {
        /* Print the content of the all files. */
        int i;
        for (i = 0; i < number_of_files; i++)
        {
            char *extension_ptr = strstr((*file_list)[i].filename, ".");
            int naked_filename_len = strlen((*file_list)[i].filename) - strlen(extension_ptr);
            char *naked_filename = STR_ALLOC(naked_filename_len + 1);

            strncpy(naked_filename, (*file_list)[i].filename, naked_filename_len);
            naked_filename[naked_filename_len] = '\0';

            printf("Num: %d\n", (*file_list)[i].id);
            printf("Name: %s\n", naked_filename);
            printf("Text: %s\n", (*file_list)[i].content);

            free(naked_filename);
        }
    }
    else if (CONTAINS(command, " -n "))
    {
        char *name = STR_ALLOC(strlen(command));
        char *name_ptr = strstr(command, " -n ");
        if (CONTAINS(command, " -t"))
        {
            /* Print the content of given file. */
            char *t_ptr = strstr(command, " -t");
            strncpy(name, name_ptr + 4, t_ptr - name_ptr - 4);
            name[t_ptr - name_ptr - 4] = '\0';
            name = STR_REALLOC(name, strlen(name) + 1);
            name = trim(name);

            int i = index_of(file_list, number_of_files, name);
            if (i != NOT_FOUND)
            {
                printf("Text: %s\n", (*file_list)[i].content);
            }
            else
                printf("[ERROR] File with the name \"%s\" not found.\n", name);
        }
        else if (CONTAINS(command, " -cw "))
        {
            /* Print the number of occurrence of the given the word in the given file. */
            char *word = strstr(command, " -cw ") + 5;
            strncpy(name, name_ptr + 4, word - name_ptr - 9);
            name[word - name_ptr - 9] = '\0';
            name = STR_REALLOC(name, strlen(name) + 1);
            name = trim(name);

            int i = index_of(file_list, number_of_files, name);
            if (i != NOT_FOUND)
            {
                printf("Text: %s\n", (*file_list)[i].content);
                printf("Number Of Occurrence of \"%s\" : %d\n", word, count_substring((*file_list)[i].content, word));
            }
            else
                printf("[ERROR] File with the name \"%s\" not found.\n", name);
        }
        else if (CONTAINS(command, " -cs"))
        {
            /* Print the number of sentences in the given file. */
            char *p_ptr = strstr(command, " -cs");
            strncpy(name, name_ptr + 4, p_ptr - name_ptr - 4);
            name[p_ptr - name_ptr - 4] = '\0';
            name = STR_REALLOC(name, strlen(name) + 1);
            name = trim(name);

            int i = index_of(file_list, number_of_files, name);
            if (i != NOT_FOUND)
            {
                char *content = (*file_list)[i].content;
                int num_sent = count_substring(content, ".");
                num_sent += count_substring(content, "!");
                num_sent += count_substring(content, "?");
                printf("Number Of Sentences : %d\n", num_sent);
            }
            else
                printf("[ERROR] File with the name \"%s\" not found.\n", name);
        }
        free(name);
    }
}

void execute_command(char *command, File **file_list, int *number_of_files, int *file_creations)
{
    if (strlen(command) > 0)
    {
        printf("%s\n", command);

        char *command_cpy = STR_ALLOC(strlen(command) + 1);
        strcpy(command_cpy, command);

        char **splt = split(command_cpy, " ");
        char *base_command = splt[0];

        if (EQUALS(base_command, "create"))
            parse_create_command(file_list, number_of_files, file_creations, command);

        else if (EQUALS(base_command, "delete"))
            parse_delete_command(file_list, number_of_files, command);

        else if (EQUALS(base_command, "append"))
            parse_append_command(file_list, *number_of_files, command);

        else if (EQUALS(base_command, "remove"))
            parse_remove_command(file_list, *number_of_files, command);

        else if (EQUALS(base_command, "replace"))
            parse_replace_command(file_list, *number_of_files, command);

        else if (EQUALS(base_command, "print"))
            parse_print_command(file_list, *number_of_files, command);

        else
            printf("[ERROR] Invalid command: %s\n", command);

        free(command_cpy);
        free(splt);
    }
}

/* 
 * Starts the execution of commands in given input,
 * line by line. 
 */
void start_execution(File **file_list, int *number_of_files, int *file_creations, char *input)
{
    char *line = input;
    while (line != NULL)
    {
        char *next_line = strchr(line, '\n');
        if (next_line != NULL)
            *next_line = '\0';

        char *command = STR_ALLOC(strlen(line) + 1);
        strcpy(command, line);
        command[strlen(line)] = '\0';

        execute_command(command, file_list, number_of_files, file_creations);

        free(command);

        line = (next_line != NULL) ? (next_line + 1) : NULL;
    }
}

int main(int argc, char **argv)
{
    char *filename = argv[1];
    char *raw_commands = read_file(filename);

    int number_of_files = 0;
    int file_creations = 0;
    File *file_list = (File *)calloc(1, sizeof(File));

    /* Re-routing the standart output to "output.txt". */
    freopen("output.txt", "w", stdout);

    /* 
     * Here, I needed to use double pointer for "file_list",
     * to be able to modify the local variable "file_list" in main function.
     * 
     * I also needed to pass an additional variable "file_creations" to keep track
     * of number of file creations in order to print the output similar to the example output.
     */
    start_execution(&file_list, &number_of_files, &file_creations, raw_commands);

    free(raw_commands);

    int i;
    for (i = 0; i < number_of_files; i++)
    {
        free(file_list[i].filename);
        free(file_list[i].content);
    }
    free(file_list);

    fclose(stdout);

    return EXIT_SUCCESS;
}
